const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
    const autoprefixer = require('autoprefixer');

function getEntrySources(sources) {
    if (process.env.NODE_ENV !== 'production') {
        sources.push('webpack-hot-middleware/client');
        sources.push('webpack-dev-server/client?http://localhost:3000');
        sources.push('webpack/hot/dev-server');
    }

    return sources;
}

function getCSSLoader() {
    if (process.env.NODE_ENV === 'production') {
        return {
            test: /\.[s]?css$/,
            loader: ExtractTextPlugin.extract('style', 'css!sass!postcss')
        }
    } else {
        return {
            test: /\.[s]?css$/,
            loaders: ['style', 'css?sourceMap', 'sass?sourceMap', 'postcss']
        }
    }
}

function getPlugins() {
    const plugins = [
        new ExtractTextPlugin('style.css', {
            allChunks: true
        }),
        new webpack.NoErrorsPlugin()
    ];
    
    if (process.env.NODE_ENV === 'production') {
        plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            })
        )
    }
    
    return plugins;
}

module.exports = {
    entry: getEntrySources([
        './src/main.js'
    ]),
    output: {
        path: path.join(__dirname, 'www'),
        filename: 'bundle.js',
        publicPath: '/'
    },
    resolve: {
        extensions: ['', '.jsx', '.scss', '.js', '.json'],
        modulesDirectories: [
            'node_modules'
        ]
    },
    module: {
        loaders: [
            {
                test: /\.js[x]?$/,
                loaders: ['react-hot', 'babel'],
                exclude: /node_modules/
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            getCSSLoader(),
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader'
            }
        ]
    },
    postcss: [autoprefixer],
    plugins: getPlugins()
};
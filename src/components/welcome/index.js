import React from 'react'
import Paper from 'material-ui/Paper';

import './welcome.scss'

export default ({user}) => {
    return (
        <div className="welcome">
            <Paper className="content" zDepth={3}>
                Welcome {user}
            </Paper>
        </div>
    );
};
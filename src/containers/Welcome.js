import React from 'react'
import reactStamp from 'react-stamp'

import Welcome from '../components/welcome'

export default reactStamp(React).compose({
    render() {
        return (
            <Welcome user="Awesome Developer"/>
        );
    }
});
import React from 'react'
import reactStamp from 'react-stamp'

import Links from '../router/links'

export default reactStamp(React).compose({
    componentWillMount() {
        this.toWelcome();
    },
    
    render() {
        return null;
    }
}, Links);
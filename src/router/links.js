import React from 'react'

import history from '../router/history'

export default {
    toWelcome() {
        history.replace("/welcome");
    }
};
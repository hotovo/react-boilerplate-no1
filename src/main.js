import React from 'react'
import ReactDOM from 'react-dom'

import injectTapEventPlugin from 'react-tap-event-plugin';
import {orange500, orange700} from 'material-ui/styles/colors'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import {Router, Route} from 'react-router'
import history from './router/history'

import 'velocity-animate'
import 'velocity-animate/velocity.ui'

injectTapEventPlugin();

import 'flexboxgrid'
import './main.scss'

import Start from './containers/Start'
import Welcome from './containers/Welcome'

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: orange500,
        primary2Color: orange700

    }
});

ReactDOM.render(
    <MuiThemeProvider muiTheme={muiTheme}>
        <Router history={history}>
            <Route path="/" component={Start}/>
            <Route path="/welcome" component={Welcome}/>
        </Router>
    </MuiThemeProvider>,
    document.getElementById("root")
);